﻿var visualizationModule = (function () {
    var mazeContainerSelector = '#mazeContainer';

    function drawMaze(mazeArray) {
        for (var i = 0; i < mazeArray.length; i++) {
            var row = $('<div>', { "class": 'row' });
            for (var j = 0; j < mazeArray[i].length; j++) {
                var symbol = mazeArray[i][j];
                var block;

                if (symbol === '*') {

                    block = $('<div>', { id: 'x' + i + 'y' + j, "class": 'block' });
                } else if (symbol === 's') {
                    block = $('<div>', { id: 'x' + i + 'y' + j, "class": 'player' });
                } else if (symbol === 'c') {
                    block = $('<div>', { id: 'x' + i + 'y' + j, "class": 'crate' });
                } else if (symbol === 'f') {
                    block = $('<div>', { id: 'x' + i + 'y' + j, "class": 'finish block' });
                } else {
                    block = $('<div>', { id: 'x' + i + 'y' + j, "class": 'path' });
                }

                block.appendTo(row);
            }

            row.appendTo($(mazeContainerSelector));
        }
    }

    function hideCrate(playerPosition) {
        $('#' + playerPosition).removeClass('crate');
    }

    function revealExit() {
        $('.finish').removeClass('block');
    }

    return {
        drawMaze: drawMaze,
        hideCrate: hideCrate,
        revealExit: revealExit
    }
})();