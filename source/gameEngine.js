﻿var gameEngine = (function () {
    var mazeArray;

    function run() {
        mazeArray = getMaze();
        drawMaze(mazeArray);
        passwordModule.showPassword();

    }

    function getMaze() {
        return mazeModule.getMaze();
    }

    function drawMaze(mazeArray) {
        visualizationModule.drawMaze(mazeArray);
    }

    $("#startBtn").click(function () {
        playerModule.init(mazeArray);
        assignKeyHandlersForMovement();
        displayModule.toggleStartBtn();
    });

    $("#submitBtn").click(function () {
        var answer = $("#answerInput").val();
        $("#answerInput").val('');
        questionModule.checkAnswer(answer);
    });

    function assignKeyHandlersForMovement() {
        $(document).keydown(function (e) {
            switch (e.which) {
                case 37: // left
                    playerModule.moveLeft();
                    break;

                case 38: // up
                    playerModule.moveUp();
                    break;

                case 39: // right
                    playerModule.moveRight();
                    break;

                case 40: // down
                    playerModule.moveDown();
                    break;

                default: return; // exit this handler for other keys
            }
            e.preventDefault(); // prevent the default action (scroll / move caret)
        });
    }

    function handleCorrectAnswer() {
        passwordModule.revealNextSymbol();
        displayModule.toggleQuestionAndRulePanes();
        visualizationModule.hideCrate(playerModule.getPlayerCoordinates());
    }

    function handleWrongAnswer() {
        playerModule.decreaseLifes();
        questionModule.askQuestion();
    }

    function gameOver() {
        alert("Game Over!");
        location.reload();
    }

    function gameWon() {
        alert("Congratulations you Won with result:  " + playerModule.getSteps() + "!");
        location.reload();
    }

    return {
        run: run,
        handleCorrectAnswer: handleCorrectAnswer,
        handleWrongAnswer: handleWrongAnswer,
        gameOver: gameOver,
        gameWon: gameWon
    }

})();

gameEngine.run();