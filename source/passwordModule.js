﻿var passwordModule = (function () {
    var password = "dodjo";
    var shownPassword = ['*', '*', '*', '*', '*'];
    var symbolsShown = 0;

    function showPassword() {
        $("#password").text("Password: " + shownPassword.join(' '));
    }

    function revealNextSymbol() {
        shownPassword[symbolsShown] = password[symbolsShown];
        showPassword();
        symbolsShown += 1;

        if (symbolsShown === password.length) { 
            visualizationModule.revealExit();
        } 
    }

    return {
        revealNextSymbol: revealNextSymbol,
        showPassword: showPassword
    }

})();