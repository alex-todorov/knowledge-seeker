﻿var displayModule = (function () {

    function toggleStartBtn() {
        $("#startBtn").toggle();
    }

    function toggleQuestionAndRulePanes() {
        $("#rulesContainer").toggle();
        $("#questionContainer").toggle();

    }

    return {
        toggleStartBtn: toggleStartBtn,
        toggleQuestionAndRulePanes: toggleQuestionAndRulePanes
    }

})();