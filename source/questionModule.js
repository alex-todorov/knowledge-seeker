﻿var questionModule = (function () {
    var question;
    var questionsAsked = [];

    var questions = [
        { q: "What is the name of the wizard at the court of King Arthur? ", a: "Merlin", imageName: "Merlin.jpg" },
        { q: "Who is the bear in The Jungle Book?  ", a: "Baloo", imageName: "Baloo.jpg" },
        { q: "What was the name of Harry Potter’s pet owl?  ", a: "Hedwig", imageName: "Hedwig.jpg" },
        { q: "Sheriff Woody Pride and Buzz Lightyear are major characters in which series of films?  ", a: "Toy Story", imageName: "ToyStory.jpg" },
        { q: "What is the name of the hero in The Lion King?  ", a: "Simba", imageName: "Simba.jpg" },
        { q: "What is the most expensive property on a standard British monopoly board? ", a: "Mayfair", imageName: "Mayfair.jpg" },
        { q: "Ping-pong is an alternative name for which sport? ", a: "Table Tennis", imageName: "TableTennis.jpg" },
        { q: "What is the largest planet in the solar system?", a: "Jupiter", imageName: "Jupiter.jpg" },
        { q: "Which planet is known as “The Red Planet”?", a: "Mars", imageName: "Mars.jpg" },
        { q: "What has the scientific formula H2O?", a: "Water", imageName: "Water.jpg" },
        { q: "The name for which group of prehistoric animals comes from the Greek for “terrible lizard”?", a: "Dinosaur", imageName: "Dinosaur.jpg" },
        { q: "What is the object hit by the players in ice hockey called? ", a: "Puck", imageName: "Puck.jpg" },
        { q: "How many players are participating in a baseball team?", a: "Nine", imageName: "Nine.jpg" },
        { q: "Which Italian city is famous for its canals?", a: "Venice", imageName: "Venice.jpg" },
        { q: "What is the line of latitude that runs around the centre of the world called?", a: "Equator", imageName: "Equator.jpg" }
    ];

    function askQuestion() {
        question = getRandomQuestion();
        $('#questionText').text(question.q);
        $("#questionImage").attr("src", 'Content/Images/' + question.imageName);
    }

    function getRandomQuestionNumber() {
        var number = Math.floor((Math.random() * questions.length - 1) + 1);
        //TODO: check if this question was already asked.
        return number;
    }

    function getRandomQuestion() {
        var questionId = getRandomQuestionNumber();
        questionsAsked.push(questionId);
        var question = questions[questionId];
        questions.splice(questionId, 1);
        return question;
    }

    function getCurrentQuestion() {
        return question;
    }

    function checkAnswer(answer) {
        if (answer.toLowerCase() === question.a.toLowerCase()) {
            console.log("Correct answer");
            gameEngine.handleCorrectAnswer();
        } else {
            console.log("Wrong answer");
            gameEngine.handleWrongAnswer();
        }
    }

    return {
        getRandomQuestion: getRandomQuestion,
        askQuestion: askQuestion,
        getCurrentQuestion: getCurrentQuestion,
        checkAnswer: checkAnswer
    }

})();