﻿var playerModule = (function () {
    var mazeArray = [];
    var positionX = 1;
    var positionY = 13;
    var stepsCount = -1;
    var lifes = 3;

    function init(mazeArr) {
        mazeArray = mazeArr;
        mangeSteps();
        manageLifes();
    }

    function moveUp() {
        if (mazeArray[positionY - 1][positionX] !== '*') {
            $("#x" + positionY + 'y' + positionX).removeClass('player').addClass('path');;
            $("#x" + (positionY - 1) + 'y' + positionX + '').addClass('player');
            positionY += -1;
            mangeSteps();
            checkPosition();
        }
    }

    function moveDown() {
        if (mazeArray[positionY + 1][positionX] !== '*') {
            $("#x" + positionY + 'y' + positionX).removeClass('player').addClass('path');;
            $("#x" + (positionY + 1) + 'y' + positionX + '').addClass('player');
            positionY += 1;
            mangeSteps();
            checkPosition();
        }
    }

    function moveLeft() {
        if (mazeArray[positionY][positionX - 1] !== '*') {
            $("#x" + positionY + 'y' + positionX).removeClass('player').addClass('path');;
            $("#x" + positionY + 'y' + (positionX - 1) + '').addClass('player');
            positionX += -1;
            mangeSteps();
            checkPosition();
        }
    }

    function moveRight() {
        if (mazeArray[positionY][positionX + 1] !== '*') {
            $("#x" + positionY + 'y' + positionX).removeClass('player').addClass('path');
            $("#x" + positionY + 'y' + (positionX + 1) + '').addClass('player');
            positionX += 1;
            mangeSteps();
            checkPosition();
        }
    }

    function checkPosition() {

        if (mazeArray[positionY][positionX] === 'c') {
            displayModule.toggleQuestionAndRulePanes();
            questionModule.askQuestion();
        } else if (mazeArray[positionY][positionX] === 'f') {
            gameEngine.gameWon();
        }
    }

    function mangeSteps() {
        stepsCount++;
        $('#steps').text('Steps count: ' + stepsCount);
    }

    function manageLifes() {
        $('#lifes').text('Lifes:' + lifes);
    }

    function decreaseLifes() {
        lifes -= 1;
        manageLifes();
        if (lifes === 0) {
            gameEngine.gameOver();
        }
    }

    function getPlayerCoordinates() {
        mazeArray[positionY][positionX] = ' ';
        return 'x' + positionY + 'y' + positionX;
    }

    function getSteps() {
        return stepsCount;
    }

    return {
        init: init,
        moveUp: moveUp,
        moveDown: moveDown,
        moveLeft: moveLeft,
        moveRight: moveRight,
        decreaseLifes: decreaseLifes,
        getPlayerCoordinates: getPlayerCoordinates,
        getSteps: getSteps
    }
})();